import React from "react";
import './Buttons.css'


class Buttons extends React.Component {

  render() {



    return (
      <div className='buttons'>
        <button onClick={this.props.start} className='btn start'>Start</button>
        <button onClick={this.props.stop} className='btn stop'>Stop</button>
        <button onClick={this.props.clear} className='btn clear'>Clear</button>
      </div>
    )
  }
}

export default Buttons