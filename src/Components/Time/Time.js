import React from "react";
import './Time.css';

class Time extends React.Component {
  render() {
    let state = this.props.state;

    return (
      <div className="time">
        <div className="sec">{(state.sec >= 10) ? state.sec : '0' + state.sec}
          <span
            className='ms'>.{(state.ms >= 10) ? state.ms : '0' + state.ms}
          </span>
        </div>
      </div>
    )
  }
}

export default Time;
