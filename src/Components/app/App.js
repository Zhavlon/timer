import React from "react";
import './App.css';
import Time from "../Time/Time";
import Buttons from "../Buttons/Buttons";

class App extends React.Component {
  state = {
    sec: 0,
    ms: 0,
  }

  start = () => {
      let newMs = this.state.ms;
      let newSec = this.state.sec;
      this.my_interval = setInterval(() => {
        if (newMs === 100) {
          newSec++;
          newMs = 0;
            }
        newMs++;
        this.setState({
          sec: newSec,
          ms: newMs
        })
      }, 10)
  }

  stop = () => {
      clearInterval(this.my_interval)
  }

  clear = () => {
      this.setState({
        sec: 0,
        ms: 0,
      })
  }

  render() {
    return (
      <div className="app">
        <Time state={this.state}/>
        <Buttons clear={this.clear} stop={this.stop} start={this.start}/>
      </div>
    )
  }
}


export default App;
